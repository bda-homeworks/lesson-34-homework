package com.bda.lesson34.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


@SpringBootTest
class YamlInfoPropertiesTest {

    @Autowired
    private YamlInfoProperties classYamlInfo;

    @Test
    public void test1 () {
        assertThat(classYamlInfo.getName()).isEqualTo("Lesson34AppFromYamlFile");
        assertThat(classYamlInfo.getVersion()).isEqualTo("Lesson34AppFromYamlFile");
    }
}