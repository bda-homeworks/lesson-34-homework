package com.bda.lesson34.model;

import lombok.Data;
import lombok.Getter;

@Getter
@Data
public class ApplicationInfo {
    private final String name;
    private final String version;

    public ApplicationInfo(String name, String version) {
        this.name = name;
        this.version = version;
    }

}
