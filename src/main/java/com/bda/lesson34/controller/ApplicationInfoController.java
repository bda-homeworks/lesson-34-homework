package com.bda.lesson34.controller;


import com.bda.lesson34.config.ApplicationInfoConfiguration;
import com.bda.lesson34.model.ApplicationInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/application")
public class ApplicationInfoController {

    private final ApplicationInfoConfiguration appInfoConfig;

    @Autowired
    public ApplicationInfoController(ApplicationInfoConfiguration appInfoConfig) {
        this.appInfoConfig = appInfoConfig;
    }

    @GetMapping("/info")
    public ApplicationInfo getInfo() {
        return new ApplicationInfo(appInfoConfig.getName(), appInfoConfig.getVersion());
    }
}
