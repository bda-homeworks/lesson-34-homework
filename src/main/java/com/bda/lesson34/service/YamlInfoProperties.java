package com.bda.lesson34.service;


import com.bda.lesson34.config.YamlPropertySourceFactory;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ConfigurationProperties(prefix = "yaml")
@PropertySource(value = "classpath:properties.yml", factory = YamlPropertySourceFactory.class)
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
public class YamlInfoProperties {
    String name;
    String version;
}
