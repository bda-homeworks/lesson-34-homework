package com.bda.lesson34.service;

import com.bda.lesson34.model.Employee;
import com.bda.lesson34.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {
    private final EmployeeRepository repo;

    @Autowired
    public EmployeeService(EmployeeRepository repo) {
        this.repo = repo;
    }

    public List<Employee> getAllEmployees() {
        return repo.findAll();
    }

    public Optional<Employee> getEmployeeById(Long id) {
        return repo.findById(id);
    }

    public void deleteEmployee(Long id) {
        repo.deleteById(id);
    }

    public Employee updateEmployee(Long id, Employee updatedEmployee) {
        if (repo.existsById(id)) {
            updatedEmployee.setId(id);
            return repo.save(updatedEmployee);
        } else {
            throw new RuntimeException("Employee not found");
        }
    }

    public Employee createEmployee(Employee employee){
        return repo.save(employee);
    }
}
